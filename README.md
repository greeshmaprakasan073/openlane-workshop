# OpenLane Workshop

<!-- Day 1 Inception of Open Source EDA -->
## Day 1 Inception of Open Source EDA

### Skywater PDK Files
![](/Uploaded_Images/1.PNG)

### Invoking OpenLane
![](/Uploaded_Images/2.PNG)

### Package Importing
![](/Uploaded_Images/3.PNG)

### Design Folder Hierarchy
![](/Uploaded_Images/4.PNG)

### Configuration File
![](/Uploaded_Images/5.PNG)

### Design Prepare  
![](/Uploaded_Images/6.PNG)
![](/Uploaded_Images/7.PNG)
![](/Uploaded_Images/8.PNG)

### Run Synthesis
![](/Uploaded_Images/9.PNG)
![](/Uploaded_Images/10.PNG)

### Readme File
![](/Uploaded_Images/13.PNG)

## Day 2 Chip Floorplanning and Standard Cells

### Run FloorPlan
![](/Uploaded_Images/11.PNG)
![](/Uploaded_Images/12.PNG)

### Generated DEF
![](/Uploaded_Images/15.PNG)
![](/Uploaded_Images/14.PNG)

### Opening Layout in OpenLane

![](/Uploaded_Images/18.PNG)
![](/Uploaded_Images/16.PNG)
![](/Uploaded_Images/17.PNG)

### Run Placement
![](/Uploaded_Images/19.PNG)
![](/Uploaded_Images/20.PNG)

### Viewing Placement in OpenLane
![](/Uploaded_Images/21.PNG)

## Day 3 Design Library Cell

### Cloning the repo
![](/Uploaded_Images/22.PNG)
![](/Uploaded_Images/23.PNG)

### Magic Layout View of Inverter Standard Cell
![](/Uploaded_Images/24.PNG)
![](/Uploaded_Images/25.PNG)

#### Device Inference
![](/Uploaded_Images/26a.PNG)
![](/Uploaded_Images/26b.PNG)
![](/Uploaded_Images/26c.PNG)

### Extraction with Magic
![](/Uploaded_Images/27a.PNG)
![](/Uploaded_Images/27b.PNG)
![](/Uploaded_Images/27c.PNG)

### Modified SPICE File
![](/Uploaded_Images/32.PNG)
![](/Uploaded_Images/28a.PNG)
![](/Uploaded_Images/29.PNG)

### Transient Analysis
![](/Uploaded_Images/30.PNG)
![](/Uploaded_Images/31.PNG)

### characterization
![](/Uploaded_Images/33.PNG)

- Rise Transition Time : 2.19586e-9 - 2.15114e-9 = 4.472E-11
- Rise Delay : 2.177e-9 - 2.15e-9 = 2.7E-11
- Fall Transition Time : 4.65e-09 - 4.039e-9 = 6.11E-10
- Fall Delay : 4.0500e-9 - 4.0529e-9 = 2.9E-12

## Day 4 Layout Timing Analysis and CTS

### LEF generation of std cell

Tracks:

![](/Uploaded_Images/34.PNG)

Enable grid in magic using command grid [xspacing [yspacing [xorigin yorigin]]] 

![](/Uploaded_Images/35.PNG)

Viewing the grid we can ensure our pin placement is optimized for PnR flow:

![](/Uploaded_Images/35b.PNG)

Defining the ports of the cell

![](/Uploaded_Images/36.PNG)

Generated cell LEF file:

![](/Uploaded_Images/37.PNG)

### Including Custom Cells in OpenLANE

Include cell level liberty file in top level liberty file and reconfigure synthesis switches in the config.tcl file

![](/Uploaded_Images/38.PNG)

Overwrite previous run to include new configuration switches

![](/Uploaded_Images/85.PNG)

Add additional statements to include extra cell LEFs:

![](/Uploaded_Images/86.PNG)

Check synthesis logs to ensure cell has been integrated correctly

![](/Uploaded_Images/87.PNG)

Run has slack violations:

![](/Uploaded_Images/88.PNG)

### Fixing Slack Violations

updated the SYNTH_STRATEGY variable 

![](/Uploaded_Images/89.PNG)

slack fixed but chip area varied:

![](/Uploaded_Images/90.PNG)

![](/Uploaded_Images/91.PNG)

slack 0 even in openSTA:

![](/Uploaded_Images/93.PNG)

pre_sta.config :

![](/Uploaded_Images/109.PNG)

SDC file for sta :

![](/Uploaded_Images/110.PNG)

Cell fanout and replacement example:(Done for experimenting since got proper slck initially)

![](/Uploaded_Images/52.PNG)

run floorplan and placement with optimized netlist.

Custom cell in the design:

![](/Uploaded_Images/46.PNG)


### Clock Tree Synthesis

![](/Uploaded_Images/97.PNG)


### Anlysing new netlist in openroad

![](/Uploaded_Images/56__4_.PNG)

We need to use the typical corner for correct analysis. 

![](/Uploaded_Images/111.PNG)

After Analysis there was hold violation and setup was met:

Hold:

![](/Uploaded_Images/101.PNG)

Setup:

![](/Uploaded_Images/102.PNG)

To fix the hold violation we removed the clock buffer with driving strenght 1 from clock buffer:

![](/Uploaded_Images/100.PNG)

The violation got fixed:

Hold:

![](/Uploaded_Images/103.PNG)

Setup:

![](/Uploaded_Images/104.PNG)

clock skews:

![](/Uploaded_Images/105.PNG)



## Day 5 PDN and Routing

### PDN generation

![](/Uploaded_Images/106.PNG)

### Routing

![](/Uploaded_Images/107.PNG)

![](/Uploaded_Images/108.PNG)

































